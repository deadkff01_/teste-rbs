import React, { useEffect, useState } from 'react'
import baseService from '../../services/baseService'
import Header from '../Common/Header'
import Container from '../Common/Container'
import Loader from '../Common/Loader'
import Row from '../Common/Row'
import UserList from '../Common/UserList'

const Main = () => {
  const [isLoading, setIsloading] = useState(true)
  const [users, setUsers] = useState([])
  const [requestError, setRequestError] = useState(false)

  const getUsers = async () => {
    try {
      const githubUsers = await baseService().get('users')
      setUsers(githubUsers.data)
      setIsloading(false)
    } catch {
      setRequestError(true)
    }
  }

  useEffect(() => {
    getUsers()
  }, [])

  const removeUser = (e, userId) => {
    e.preventDefault()
    setUsers(users.filter(u => u.id !== userId))
  }

  if (requestError) {
    return <h3>Erro na request, verifique sua conexão com a internet ou recarregue a pagina.</h3>
  }

  return (
    <>
      <Header>GITHUB USERS</Header>
      {isLoading ? (
        <Loader />
      ) : (
        <Container>
          <Row>
            <UserList users={users} removeUser={removeUser} />
          </Row>
        </Container>
      )}
    </>
  )
}

export default Main
