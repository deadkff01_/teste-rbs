import React from 'react'
import ReactDOM from 'react-dom'
import Main from './Main'
import baseService from '../../services/baseService'

it('Test Gituhub request', async () => {
  try {
    const users = await baseService().get('users')
    expect(users.data).toBeDefined()
  } catch (e) {
    expect(e.response.data.message).toBe('Not Found')
  }
})

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<Main />, div)
  ReactDOM.unmountComponentAtNode(div)
})
