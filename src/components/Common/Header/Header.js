import styled from 'styled-components'

const Header = styled.header`
  width: 100%;
  height: 80px;
  background-color: #24292e;
  color: #fff;
  text-align: center;
  line-height: 80px;
  font-weight: 700;
  font-size: 20px;
`
export default Header
