import React from 'react'
import PropTypes from 'prop-types'
import UserCard from '../UserCard'

const UserList = ({ users, removeUser }) =>
  users.length > 0 ? (
    users.map(user => <UserCard data-test="user-list" user={user} key={user.node_id} removeUser={removeUser} />)
  ) : (
    <div data-test="users-not-found">Nenhum usuário encontrado</div>
  )

UserList.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
  removeUser: PropTypes.func
}

export default UserList
