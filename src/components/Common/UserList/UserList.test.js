import React from 'react'
import { configure, shallow } from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'
import UserList from './UserList'
import mockData from '../../../test/mock'

configure({ adapter: new EnzymeAdapter() })

const setup = (props = {}) => shallow(<UserList {...props} />)

it('Test UserList component', () => {
  const wrapper = setup({ users: mockData })
  const component = wrapper.find(`[data-test="user-list"]`)
  expect(component.length).toBe(4)
})

it('Test UserList show not found users message', () => {
  const wrapper = setup({ users: [] })
  const component = wrapper.find(`[data-test="users-not-found"]`)
  expect(component.length).toBe(1)
})
