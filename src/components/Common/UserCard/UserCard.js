import React, { useState } from 'react'
import PropTypes from 'prop-types'
import * as styles from './styles'

const UserCard = ({ user, removeUser }) => {
  const [showIconClose, setShowIconClose] = useState(false)

  return (
    <styles.ColMD4>
      <styles.Card
        onMouseOver={() => setShowIconClose(true)}
        onFocus={() => setShowIconClose(true)} // Accessibility https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/mouse-events-have-key-events.md
        onMouseLeave={() => setShowIconClose(false)}
        alt={`Card of ${user.login}`}
      >
        <styles.CloseButton onClick={e => removeUser(e, user.id)} show={showIconClose} />
        <styles.ImgCard src={user.avatar_url} title={user.login} alt={user.login} />
        <styles.CardBody>
          <h1>{user.login}</h1>
          <styles.SocialButton alt="Github" href={user.html_url}>
            Github Profile
          </styles.SocialButton>
          <styles.SocialButton alt="Gist" href={`https://gist.github.com/${user.login}`}>
            Gist
          </styles.SocialButton>
        </styles.CardBody>
      </styles.Card>
    </styles.ColMD4>
  )
}

UserCard.propTypes = {
  user: PropTypes.object.isRequired,
  removeUser: PropTypes.func
}

export default UserCard
