import styled from 'styled-components'
import iconClose from '../../../assets/closeIcon.png'

export const ColMD4 = styled.div`
  -webkit-box-flex: 0;
  -ms-flex: 0 0 33.333333%;
  flex: 0 0 33.333333%;
  max-width: 33.333333%;
  @media (max-width: 768px) {
    max-width: 100%;
    flex: 0 0 100%;
  }
`
export const Card = styled.div`
  position: relative;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  -ms-flex-direction: column;
  flex-direction: column;
  min-width: 0;
  word-wrap: break-word;
  background-color: #fff;
  background-clip: border-box;
  border: 1px solid rgba(0, 0, 0, 0.125);
  border-radius: 0.25rem;
  margin: 10px 10px;
  box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, .05);
}
`
export const ImgCard = styled.img`
  width: 120px;
  height: 120px;
  border-radius: 50%;
  position: relative;
  margin: 10px auto;
  display: block;
`
export const CardBody = styled.div`
  -webkit-box-flex: 1;
  -ms-flex: 1 1 auto;
  flex: 1 1 auto;
  padding: 0px;
  text-align: center;
  h1 {
    text-align: center;
    font-size: 20px;
  }
`
export const SocialButton = styled.a`
  border: 1px solid #000;
  padding: 10px;
  margin: 0px 5px 10px 5px;
  width: auto;
  display: inline-block;
  border-radius: 0.25rem;
  cursor: pointer;
  outline: 0;
  -webkit-text-decoration: none;
  text-decoration: none;
  color: #000;
  background: transparent;
  -webkit-transition: all 0.5s ease;
  -moz-transition: all 0.5s ease;
  -o-transition: all 0.5s ease;
  transition: all 0.5s ease;
  z-index: 2;
  &:hover {
    background: #000;
    color: #fff;
  }
`
export const CloseButton = styled.div`
  position: absolute;
  height: 20px;
  width: 20px;
  background: url('${iconClose}');
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  top: 10px;
  right: 10px;
  display:${({ show }) => (show ? 'block' : 'none')};
  @media (max-width: 576px) {
    display: block;
  }
`
