import styled from 'styled-components'
import loaderImg from '../../../assets/loader.gif'

const Loader = styled.div`
  background: url('${loaderImg}');
  background-position: center;
  background-repeat: no-repeat;
  background-size: unset;
  position: absolute;
  bottom: 0px;
  top: 0px;
  width: 100%;
  height: 100%;
`

export default Loader
