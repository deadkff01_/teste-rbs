import axios from 'axios'
import env from '../config/env'

const baseService = () => {
  const headers = { 'Content-Type': 'application/json' }
  const instance = axios.create({
    baseURL: env.REACT_APP_API_ENDPOINT,
    timeout: 30000,
    maxRedirects: 0,
    headers
  })
  return instance
}

export default baseService
